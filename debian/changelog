gdpc (2.2.5-16) unstable; urgency=medium

  * Add build support for loongarch64
    Closes: #1058674
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 15 Dec 2023 18:31:16 +0100

gdpc (2.2.5-15) unstable; urgency=medium

  * create icon gdpc.png for the application; this should make it easier for
    desktop users to recognize gdpc menu entry and launchers
  * debian/control:
      - update uploader address
      - mark gdpc-examples Multi-Arch: foreign
  * Standards-Version: 4.6.0 (routine-update)

 -- Étienne Mollier <emollier@debian.org>  Sun, 22 Aug 2021 23:13:37 +0200

gdpc (2.2.5-14) unstable; urgency=medium

  * Remove ppc64el from autopkgtest
    Closes: #981876

 -- Andreas Tille <tille@debian.org>  Fri, 21 May 2021 14:15:14 +0200

gdpc (2.2.5-13) unstable; urgency=medium

  * d/{,t/}control: remove all 32-bits architectures support; see #981876 for
    the context (also, refer to #982103 about s390x exclusion)

 -- Étienne Mollier <etienne.mollier@mailoo.org>  Mon, 08 Feb 2021 21:36:52 +0100

gdpc (2.2.5-12) unstable; urgency=medium

  * Add myself to uploaders.
  * d/{,t/}control: exclude i386 and s390x from supported architectures.

 -- Étienne Mollier <etienne.mollier@mailoo.org>  Sat, 06 Feb 2021 17:27:12 +0100

gdpc (2.2.5-11) unstable; urgency=medium

  * Nelson A. de Oliveira left Debian so remove him from Uploaders
    (Thanks Nelson for your work on this package)
    Closes: #962108
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Sun, 20 Dec 2020 18:39:53 +0100

gdpc (2.2.5-10) unstable; urgency=medium

  [ Graham Inggs ]
  * Fix autopkgtest
    Closes: #941581

  [ Andreas Tille ]
  * Add debug output to autopkgtest to enable catching failures
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 12 (routine-update)
  * Trim trailing whitespace.

 -- Andreas Tille <tille@debian.org>  Wed, 29 Jan 2020 17:00:44 +0100

gdpc (2.2.5-9) unstable; urgency=medium

  * Homepage seems to have vanished.  Set homepage to what was found at
    2018-02-12 by web.archive.org
  * Fake watch file
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Thu, 01 Nov 2018 15:29:12 +0100

gdpc (2.2.5-8) unstable; urgency=medium

  * Add missing xauth to debian/tests/control

 -- Andreas Tille <tille@debian.org>  Mon, 11 Dec 2017 15:59:29 +0100

gdpc (2.2.5-7) unstable; urgency=medium

  * Do not use the build architecture pkg-config (Thanks for the patch to
    Helmut Grohne <helmut@subdivi.de>)
    Closes: #883892
  * Standards-Version: 4.1.2
  * debhelper 10
  * Activate autopkgtest using xvfb - no idea whether this makes real sense
    but us it as an experiment
  * Secure URI for watch file

 -- Andreas Tille <tille@debian.org>  Sat, 09 Dec 2017 07:53:15 +0100

gdpc (2.2.5-6) unstable; urgency=medium

  * Team upload

  [ Andreas Tille ]
  * Move packaging from SVN to Git
  * Fix spelling

  [ Canberk Koç ]
  * Test script added.

  [ Sascha Steinbiss ]
  * Fix segfaults

 -- Canberk Koç <canberkkoc@gmail.com>  Sun, 24 Jul 2016 01:39:50 +0300

gdpc (2.2.5-5) unstable; urgency=medium

  * Fix implicit function declarations (Thanks for the patch to  Logan Rosen
    <logan@ubuntu.com>)
    Closes: #815041
  * cme fix dpkg-control
  * hardening=+all

 -- Andreas Tille <tille@debian.org>  Tue, 12 Jul 2016 09:47:36 +0200

gdpc (2.2.5-4) unstable; urgency=medium

  * Remove menu file
  * cme fix dpkg-control
  * Remove outdated d/README.source
  * DEP5 fix
  * Propagate hardening options

 -- Andreas Tille <tille@debian.org>  Sun, 24 Jan 2016 21:44:53 +0100

gdpc (2.2.5-3) unstable; urgency=low

  * debian/control:
     - cme fix dpkg-control
     - debhelper 9
     - Use anonscm in Vcs fields
  * debian/copyright: DEP5
  * debian/patches/40_fix_gcc4.8_build.patch:
    Fix build with gcc4.8
    Closes: #713652

 -- Andreas Tille <tille@debian.org>  Tue, 15 Oct 2013 14:55:27 +0200

gdpc (2.2.5-2) unstable; urgency=low

  * Do not use -DGDK_DISABLE_DEPRECATED (Thanks to Colin Watson
    <cjwatson@ubuntu.com> for the patch)
    Closes: #642094 (LP: #770768)
  * Standards-Version: 3.9.2 (no changes needed)
  * debian/source/format: 3.0 (quilt)
  * Debhelper 8 (control+compat)

 -- Andreas Tille <tille@debian.org>  Fri, 09 Dec 2011 08:22:35 +0100

gdpc (2.2.5-1) unstable; urgency=low

  [ Charles Plessy ]
  * Removed category 'Utility' in debian/gdpc.desktop as it was making the
    entry appear in the 'Accessories' section of the GNOME menu.

  [ Andreas Tille ]
  * New upstream version
  * debian/control:
    - Added myself to uploaders
    - Standards-Version: 3.8.3 (added README.source)
    - Debhelper 7
    - use quilt instead of simple-patchsys
  * debian/rules: short rules file using dh

 -- Andreas Tille <tille@debian.org>  Sun, 04 Oct 2009 09:59:31 +0200

gdpc (2.2.4-3) unstable; urgency=low

  * Updated gdpc homepage (Closes: #451920). Thanks Charles!
  * Updated Standards-Version;
  * Changed Maintainer to the Debian-Med Packaging Team;
  * Added Vcs-* fields.

 -- Nelson A. de Oliveira <naoliv@debian.org>  Thu, 21 Feb 2008 23:08:51 -0300

gdpc (2.2.4-2) unstable; urgency=low

  * Fix FTBS with newer libgtk2.0-0 versions (Launchpad #136973)
    Thanks to Stephan Hermann
    - Updated patches/Makefile.diff.
  * Updated debhelper compat level to 5;
  * Updated my email address;
  * Updated Standards-Version to 3.7.2;
  * Updated deprecated ${Source-Version} to ${source:Version};
  * Updated menu file;
  * Updated watch file;
  * Updated FSF address in copyright file;
  * Added desktop file (Launchpad #36377) - Thanks to Phil Bull.

 -- Nelson A. de Oliveira <naoliv@debian.org>  Mon, 03 Sep 2007 12:06:20 -0300

gdpc (2.2.4-1) unstable; urgency=low

  * Initial Release (Closes: #306028).

 -- Nelson A. de Oliveira <naoliv@gmail.com>  Sat, 23 Apr 2005 15:50:09 -0300
