Source: gdpc
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Étienne Mollier <emollier@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libgtk2.0-dev
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/gdpc
Vcs-Git: https://salsa.debian.org/med-team/gdpc.git
Homepage: https://web.archive.org/web/20180212152753/http://www.frantz.fi/software/gdpc.php
Rules-Requires-Root: no

Package: gdpc
Architecture: any-amd64 arm64 loong64 mips64el ppc64el riscv64
Depends: ${shlibs:Depends},
         ${misc:Depends}
Suggests: gdpc-examples (>= ${source:Version}),
          gqview
Description: visualiser of molecular dynamic simulations
 gpdc is a graphical program for visualising output data from
 molecular dynamics simulations. It reads input in the standard xyz
 format, as well as other custom formats, and can output pictures of
 each frame in JPG or PNG format.

Package: gdpc-examples
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: gdpc (>= ${source:Version})
Description: example files for the gdpc program
 gpdc is a graphical program for visualising output data from
 molecular dynamics simulations. It reads input in the standard xyz
 format, as well as other custom formats, and can output pictures of
 each frame in JPG or PNG format.
 .
 This package contains examples to be used by the gdpc program.
