Summary:	A program for visualising molecular dynamics simulations data
Name:		gdpc
Version:	2.2.5
Release:	1
License:	GPLv2+
Group:		Applications/Engineering
URL:		http://www.frantz.fi/software/gdpc.php
Source0:	http://www.frantz.fi/software/gdpc-%{version}.tar.gz
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	gtk2-devel
BuildRequires:	pkgconfig

%description
gdpc is a program for visualising molecular dynamic simulations,
it is a very versatile program and could easily be used for other
purposes. gdpc reads xyz input as well as custom formats and can
output pictures of each frame.

%prep
%setup -q

%build
make CFLAGS="%{optflags}" %{?_smp_mflags}

%install
rm -rf %{buildroot} 
install -D -p -m 755 gdpc %{buildroot}%{_bindir}/gdpc

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README liquid.test manytypes.test md.test gpl.txt
%{_bindir}/gdpc


%changelog
* Wed Apr 22 2009 Jussi Lehtola <jussi.lehtola@iki.fi> - 2.2.5-1
- Conversion of spec to Fedora.
* Tue Apr 21 2009 Jonas Frantz <jonas.frantz@welho.com> - 2.2.5
- Updated code to GTK+ 2.16 compliance
* Sat Dec 11 2004 Jonas Frantz <frantz@acclab.helsinki.fi>
- Version 2.2.4 released
* Wed Dec 8 2004 Jonas Frantz <frantz@acclab.helsinki.fi>
- Version 2.2.3.1 released
* Mon Dec 6 2004 Jonas Frantz <frantz@acclab.helsinki.fi>
- Version 2.2.3 released
* Sat Aug 9 2003 Jonas Frantz <jonas.frantz@helsinki.fi>
- Initial build

