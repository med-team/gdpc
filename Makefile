CC = gcc
FLAGS=$(CFLAGS) -g -Wall `pkg-config --cflags gtk+-2.0 gthread-2.0` -DG_DISABLE_DEPRECATED -DGDK_DISABLE_DEPRECATED -DGDK_PIXBUF_DISABLE_DEPRECATED -DGTK_DISABLE_DEPRECATED
LIBS=`pkg-config --libs gtk+-2.0 gthread-2.0`
bindir ?= /usr/bin

.c.o:
	$(CC) -c $(FLAGS) $(CPPFLAGS) $<

gdpc: main.o colors.o sort.o drawatoms.o readinput.o init.o rotate.o setup.o Makefile
	$(CC) $(CFLAGS) -o gdpc main.o colors.o drawatoms.o init.o sort.o rotate.o setup.o readinput.o $(LIBS)

main.o: main.c parameters.h

colors.o: colors.c parameters.h

readinput.o: readinput.c parameters.h

drawatoms.o: drawatoms.c parameters.h

init.o: init.c parameters.h

sort.o: sort.c parameters.h

rotate.o: rotate.c parameters.h

setup.o: setup.c parameters.h tooltips.h

clean:
	rm *.o gdpc

install:
	install -p -m 755 -D gdpc $(bindir)/gdpc

uninstall:
	rm $(bindir)/gdpc
