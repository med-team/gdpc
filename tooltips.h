/*

gdpc - a program for visualising molecular dynamic simulations
Copyright (C) 2000 Jonas Frantz

    This file is part of gdpc.

    gdpc is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    gdpc is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


Authors email : jonas.frantz@helsinki.fi

*/

gchar timedeltt[] =	"With this entrybox you can decide what unit gdpc will be looking for "
			"in the commentline of the xyz header, when trying to determine frametime.";

gchar inputfilett[] =	"This entrybox contains the name of the file to be used for input. ";

gchar drawsizett[] =	"With these two spinners you can resize the drawingboard.";

gchar cubett[] =	"With this spinner you can set the used size of the input space.";

gchar mintt[] =		"This spinner sets minima border of input space.";

gchar maxtt[] =		"This spinner sets maxima border of input space.";

gchar xcoltt[] =	"This number represents the column in which the x coordinates are read from.";

gchar ycoltt[] =	"This number represents the column in which the y coordinates are read from.";

gchar zcoltt[] =	"This number represents the column in which the z coordinates are read from.";

gchar tcoltt[] =	"This number represents the column in which the t coordinates are read from.";

gchar coltypett[] =	"With this button you can choose to color the atoms according to atom type, "
			"this only works with xyz input file format.";
